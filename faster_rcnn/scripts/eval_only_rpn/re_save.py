from mindspore.train.serialization import load_checkpoint, load_param_into_net, save_checkpoint
from src.FasterRcnn.faster_rcnn_r50 import Faster_Rcnn_Resnet50
from src.config import config

#1.这是一个规避脚本，只需要执行一次，得到新的ckpt之后就不需要执行了。
#2.采用如下方式规避的愿意是fasterrcnn的loss和opt是自定义的，经过train之后的模型结构保存顺序变化，无法与量化网络匹配

if __name__ == "__main__":
    net = Faster_Rcnn_Resnet50(config=config) #这里的net是原始fp32定义的网络结构
    net = net.set_train()
    param_dict = load_checkpoint("../fasterrcnn_ascend_v120_coco2017_official_cv_bs2_acc59.ckpt") #这里的模型是下载的原始模型
    load_param_into_net(net, param_dict)
    save_checkpoint(net, "./faster_rcnn_new.ckpt")