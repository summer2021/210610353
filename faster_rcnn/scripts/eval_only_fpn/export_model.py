from mindspore.train.serialization import load_checkpoint, load_param_into_net
from mindspore import Tensor, context, export
from src.FasterRcnn.faster_rcnn_r50 import Faster_Rcnn_Resnet50
from src.config import config
from mindspore.compression.quant import QuantizationAwareTraining
from mindspore.compression.common import QuantDtype
import numpy as np
import mindspore as ms

if __name__ == "__main__":
    net = Faster_Rcnn_Resnet50(config=config)
    #TODO:插入伪量化节点, convert fusion network to quantization aware network
    quantizer = QuantizationAwareTraining(bn_fold=False,
                                        quant_dtype=QuantDtype.INT8, 
                                        per_channel=[True, False],
                                        symmetric=[False, False])
    net = quantizer.quantize(net)

    param_dict = load_checkpoint("scripts/train/ckpt_0/faster_rcnn-3_29571.ckpt")
    load_param_into_net(net, param_dict)
    #export network
    inputs = Tensor(np.ones([1, 1, config.img_height, config.img_width]), ms.float32)
    export(net, inputs,file_name="faster_rcnn_quant-res_noupdate1", file_format="MINDIR", quant_mode="AUTO")