echo 'download train image' #打印信息。
wget http://images.cocodataset.org/zips/train2017.zip
echo 'download val image'
wget http://images.cocodataset.org/zips/val2017.zip
echo 'download train/val anno'
wget http://images.cocodataset.org/annotations/annotations_trainval2017.zip

echo 'download done, start unzipping' #数据集下载结束，打印信息
unzip train2017.zip
unzip val2017.zip
unzip annotations_trainval2017.zip