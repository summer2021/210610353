# 目录

- [Get Started](#get started)

  - [环境配置](##环境配置)

  - [数据集](##数据集)

- [快速入门](#快速入门)

- [脚本说明](#脚本说明)
  - [数据集下载](##数据集下载)
  - [预训练模型下载](##预训练模型下载)
  - [模型转换](##模型转换)
  - [量化脚本](##量化脚本)

- [测评结果](#测评结果)

- [参考](#参考)

# Get Started

## 环境配置

- 硬件（Ascend/GPU）

  - 使用Ascend处理器来搭建硬件环境。

- 获取基础镜像

  - [Ascend Hub](https://ascend.huawei.com/ascendhub/#/home)

- 安装[MindSpore1.3](https://www.mindspore.cn/install)。

- 下载数据集COCO 2017。

  1. 若使用COCO数据集，**执行脚本时选择数据集COCO。** 安装Cython和pycocotool，也可以安装mmcv进行数据处理。

     ```bash
     pip install Cython
     pip install pycocotools
     pip install mmcv==0.2.14
     ```

     在`config.py`中更改COCO_ROOT和其他您需要的设置。目录结构如下：

     ```
     .
     └─cocodataset
       ├─annotations
         ├─instance_train2017.json
         └─instance_val2017.json
       ├─val2017
       └─train2017
     ```

- 安装mindspore_hub1.3.0

  用于从mindspore hub上下载预训练模型，可采用如下命令安装：

  ```bash
  pip install https://ms-release.obs.cn-north-4.myhuaweicloud.com/1.3.0/Hub/any/mindspore_hub-1.3.0-py3-none-any.whl --trusted-host ms-release.obs.cn-north-4.myhuaweicloud.com -i https://pypi.tuna.tsinghua.edu.cn/simple
  ```

## 数据集

使用的数据集：[COCO 2017](https://cocodataset.org/)

- 数据集大小：19G
  - 训练集：18G，118,000个图像
  - 验证集：1G，5000个图像
  - 标注集：241M，实例，字幕，person_keypoints等
- 数据格式：图像和json文件
  - 注意：数据在dataset.py中处理。

# 快速入门

1. 使用download_coco2017.sh脚本下载coco2017数据集，解压缩，按照config.py中要求存放数据集。

2. 使用download_pretrained.py脚本下载预训练模型。你也可以在[mindspore hub](https://mindspore.cn/resources/hub/details?MindSpore/ascend/v1.2/fasterrcnn_v1.2_coco2017)手动下载。

3. 将re_save.py放置到原始faster rcnn代码train.py同级目录，更改load_checkpoint路径和save_checkpoint路径，生成新的fp32模型权重。

# 脚本说明

```c++
.
└─faster_rcnn
  ├─README.md    // Faster R-CNN相关说明
  ├─ascend310_infer  //实现310推理源代码
  ├─scripts
    ├─run_standalone_train_ascend.sh    // Ascend单机shell脚本
    ├─run_standalone_train_gpu.sh    // GPU单机shell脚本
    ├─run_distribute_train_ascend.sh    // Ascend分布式shell脚本
    ├─run_distribute_train_gpu.sh    // GPU分布式shell脚本
    ├─run_infer_310.sh    // Ascend推理shell脚本
    └─run_eval_ascend.sh    // Ascend评估shell脚本
    └─run_eval_gpu.sh    // GPU评估shell脚本
    └─eval_resnet50-12    // 量化resnet50，epoch12结果
    └─eval_resnet50-3   // 量化resnet50，epoch3结果
    └─eval_only_fpn    // 量化fpn，epoch1结果
    └─eval_only_rpn    // 量化rpn，epoch1结果
    └─eval_rpn+fpn    // 量化rpn+fpn，epoch1结果
  ├─src
    ├─FasterRcnn
      ├─__init__.py    // init文件
      ├─anchor_generator.py    // 锚点生成器
      ├─bbox_assign_sample.py    // 第一阶段采样器
      ├─bbox_assign_sample_stage2.py    // 第二阶段采样器
      ├─faster_rcnn_r50.py    // Faster R-CNN网络(changed)
      ├─fpn_neck.py    // 量化特征金字塔网络(changed)
      ├─proposal_generator.py    // 候选生成器
      ├─rcnn.py    // R-CNN网络
      ├─resnet50.py    // 量化骨干网络，允许参数更新(changed)
      ├─resnet50_noupdate.py    // 量化骨干网络，参数不更新
      ├─rcnn.py    // R-CNN网络
      ├─roi_align.py    // ROI对齐网络
      └─rpn.py    //  量化区域候选网络(changed)
    ├─aipp.cfg    // aipp 配置文件
    ├─config.py    // 总配置
    ├─dataset.py    // 创建并处理数据集
    ├─lr_schedule.py    // 学习率生成器
    ├─network_define.py    // Faster R-CNN网络定义
    └─util.py    // 例行操作
  ├─export.py    // 导出 AIR,MINDIR,ONNX模型的脚本
  ├─eval.py    // 量化评估脚本(changed)，包含参数量估计。
  ├─postprogress.py    // 310推理后处理脚本
  ├─re_save.py    // 这是一个规避脚本，只需要执行一次，得到新的ckpt之后就不需要执行了。
  └─train.py    // 量化训练脚本(changed)
 .
└─cocodataset
  └─download_coco2017.sh    // 下载coco数据集，并解压
```

## 数据集下载

1. 使用download_coco2017.sh脚本下载coco图像、标签数据集，数据集一共大约20G，耗时较长。
2. 该脚本下载完数据集，自动执行unzip命令，将下载的train2017.zip、val2017.zip、annotations_trainval2017.zip解压。

```bash
chmod a+x download_coco2017.sh
#执行下载数据集脚本
./download_coco2017.sh
```

注：若运行过程出错，可手动执行脚本中每条命令。

​	3.下载完数据集后，需要对应修改config.py中mindrecord_dir、coco_root路径。

## 预训练模型下载

采用mindspore hub的预训练模型fasterrcnn_v1.2_coco2017，进一步进行模型的量化训练，预训练模型及对应Faster RCNN代码可在[mindspore hub](https://mindspore.cn/resources/hub/details?MindSpore/ascend/v1.2/fasterrcnn_v1.2_coco2017)下载。也可用如下脚本下载：

```python
import mindspore_hub as mshub
from mindspore import context

context.set_context(mode=context.GRAPH_MODE,
                    device_target="Ascend",
                    device_id=0)

model = "mindspore/ascend/1.2/fasterrcnn_v1.2_coco2017"
# initialize the number of classes based on the pre-trained model
network = mshub.load(model)
network.set_train(False)
```

## 模型转换

1. 对应修改re_save.py中load_checkpoint的路径，即原始预训练模型所在路径。
2. 对应修改re_save.py中save_checkpoint路径，即新生成的模型路径。
3. 执行re_save.py脚本，生成新模型。

## 量化脚本

参考modelzoo模型训练脚本：

**在GPU上运行**

```bash
# 单机训练
sh run_standalone_train_gpu.sh [PRETRAINED_MODEL]

# 分布式训练
sh run_distribute_train_gpu.sh [DEVICE_NUM] [PRETRAINED_MODEL]

# 评估
sh run_eval_gpu.sh [VALIDATION_JSON_FILE] [CHECKPOINT_PATH]
```

其中 1. PRETRAINED_MODEL使用re_save.py生成的新模型;DEVICE_NUM为使用GPU核数。

2. VALIDATION_JSON_FILE为标签文件。
3. CHECKPOINT_PATH是训练后的检查点文件。

# 测评结果

针对不同层间量化敏感度，进行量化实验，包含resnet、rpn、fpn_head三层的单独量化实验。实验均采用自动量化、逐通道、非对称的量化策略，实验结果如下表：

| 量化策略    | AP    | APs   | APm   | APl   | AR    | ARs   | ARm   | ARl   |
| ----------- | ----- | ----- | ----- | ----- | ----- | ----- | ----- | ----- |
| 未量化      | 0.368 | 0.238 | 0.409 | 0.458 | 0.517 | 0.354 | 0.561 | 0.633 |
| Resnet50-12 | 0.379 | 0.241 | 0.418 | 0.488 | 0.524 | 0.347 | 0.568 | 0.655 |
| Resnet50-3  | 0.247 | 0.135 | 0.284 | 0.332 | 0.395 | 0.207 | 0.436 | 0.543 |
| Rpn         | 0.123 | 0.076 | 0.141 | 0.147 | 0.312 | 0.180 | 0.337 | 0.386 |
| Fpn_head    | 0.129 | 0.085 | 0.152 | 0.151 | 0.301 | 0.186 | 0.327 | 0.360 |
| RPN+FPN     | 0.122 | 0.079 | 0.136 | 0.144 | 0.289 | 0.175 | 0.317 | 0.348 |

​	其中AP代表平均精确度、下标s代表小物体、m代表中等大小物体、l代表大物体。AR为平均召回率指标；Resnet50-12为迭代12epoch实验结果、Resnet50-3为迭代3epoch实验结果，其余实验受限于时间及计算资源限制，仅实验迭代1epoch结果。

# 参考

modelzoo官方:[faster rcnn网络](https://gitee.com/mindspore/mindspore/tree/r1.2/model_zoo/official/cv/faster_rcnn)

mindspore hub:[faster rcnn预训练模型](https://mindspore.cn/resources/hub/details?MindSpore/ascend/v1.2/fasterrcnn_v1.2_coco2017)
